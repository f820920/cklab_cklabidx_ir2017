# Requirements

## Python Environment

* python 3.x
* pip3

## Database systems

* Sqlite 3.x +
* PostgreSQL 9.5 +

## Python Packages

* pip install nltk psycopg2
