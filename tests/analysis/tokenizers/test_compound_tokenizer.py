#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import unittest
from cklabidx.analysis.tokenizers.compound_tokenizer import CompoundTokenizer


class CompoundTokenizerTest(unittest.TestCase):

    def test_compound_tokenizer(self):

        self.tokenizer = CompoundTokenizer()

        self.assertEqual(self.tokenizer.generate_tokens("中文english 123"), ["中", "文", "english", "123"])

if __name__ == '__main__':
    unittest.main()
