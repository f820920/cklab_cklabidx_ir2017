#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#


class QueryParser(object):
    """
    The object to parse raw query into analyzed token list.

    Object format for query_dict:
    {
        "field_1": [
                "term1", "term2", "term3", ...
            ],
        "field_2": [
                "term4", "term5", "term6", ...
            ],
            ...
    }
    """

    def __init__(self, analyzer):
        self.analyzer = analyzer
        self.query_dict = {}

    def __str__(self):
        return "Query: " + str(self.query_dict)

    def add_field(self, field_name, query_string):

        # Apply analyzer
        tokens = self.analyzer.analyze(field_name, query_string)

        self.query_dict[field_name] = tokens
