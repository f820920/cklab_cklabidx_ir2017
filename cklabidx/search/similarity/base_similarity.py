#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import re
import json

from abc import ABCMeta, abstractmethod


class Similarity(metaclass=ABCMeta):

    def __init__(self, use_explain=False):
        self.index_object = None
        self.query_object = None

        self.use_explain = use_explain
        if self.use_explain:
            self.explain = self.ScoringExplain()

    def set_index_object(self, index_object):
        """
        Set Index object, called by IndexSearcher
        :param index_object:
        :return:
        """
        self.index_object = index_object

    def set_query_object(self, query_object):
        """
        Set Query object, called by IndexSearcher
        :param query_object:
        :return:
        """
        self.query_object = query_object

    def set_formula_doc_filter(self, doc_filter_re):
        """
        Set document name filter of the ScoringFormula object
        :param doc_filter_re:
        :return:
        """
        if self.use_explain:
            self.explain.set_doc_filter(doc_filter_re)

    @abstractmethod
    def update_hit_doc_list(self, field, term, hit_doc_list):
        """
        Update hit document list, call once for each term.
        :param field:
        :param term:
        :param hit_doc_list:
        :return:
        """
        pass

    @abstractmethod
    def get_scores(self, field, term_list, hit_doc_list):
        pass

    class ScoringExplain(object):
        """
        Inner class to store scoring formula of documents
        """
        def __init__(self, doc_filter_re=""):
            self.explain_dict = {}
            self.notation = ""
            self.doc_filter_re = doc_filter_re

        def __str__(self):
            if self.doc_filter_re:
                filter_re = self.doc_filter_re
            else:
                filter_re = ".*"

            expr = {"filter_re": filter_re,
                    "notation": self.notation,
                    "explain": self.explain_dict}

            return json.dumps(expr, sort_keys=True, indent=4, separators=(',', ': '),
                              ensure_ascii=False)

        def set_doc_filter(self, doc_filter_re):
            self.doc_filter_re = doc_filter_re

        def add_document(self, doc_name, explain_string, score=None):
            if self.doc_filter_re:
                if re.match(self.doc_filter_re, doc_name):
                    self.explain_dict[doc_name] = (explain_string, score)
            else:
                self.explain_dict[doc_name] = (explain_string, score)
