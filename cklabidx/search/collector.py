#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from operator import itemgetter


class Document(object):
    """
    Wrapper class for Documents
    """
    def __init__(self, doc_id, doc_score):
        self.doc_id = doc_id
        self.score = doc_score

    def __repr__(self):
        return "<Document({id}, {score})>".format(id=self.doc_id, score=self.score)


class Passage(object):
    """
    Wrapper class for Passages
    """
    def __init__(self, doc_id, passage_score, start, end):
        self.doc_id = doc_id
        self.score = passage_score
        self.start = start
        self.end = end

    def __repr__(self):
        return "<Passage({id}, {start} -- {end}, {score})>".format(
            id=self.doc_id, score=self.score, start=self.start, end=self.end
        )


class DocumentCollector(object):
    """
    The object to collect and sort all document scores.
    Return a list of Document object

        Object format for doc_scores_dict:
        {
            "field1": {
                "doc1": score1,
                "doc2": score2,
                ...
            },
            "field2": {
                "doc3": score3,
                "doc4": score4,
                ...
            }, ...
        }

    """
    def __init__(self):
        self.doc_scores_dict = {}
        self.merged_scores = {}  # Merged score in all fields. Use max() if conflict.

    def set_document_scores(self, field, scores_dict):
        self.doc_scores_dict[field] = scores_dict

    def get_field_top_documents(self, field, offset=0, counts=10):
        """
        Return top N documents in a specified field
        :param field:
        :param offset:
        :param counts:
        :return:
        """
        score_order_list = sorted(self.doc_scores_dict[field].items(), key=itemgetter(1), reverse=True)
        return self.build_output(len(score_order_list),
                                 offset,
                                 min(len(score_order_list), counts),
                                 score_order_list[offset:offset+counts])

    def get_overall_top_documents(self, offset=0, counts=10):
        """
        Return top N documents, all fields are merged
        :param offset:
        :param counts:
        :return:
        """

        for field in self.doc_scores_dict:
            conflict_keys = set(self.doc_scores_dict[field]) & set(self.merged_scores)
            conflict_dict = {}
            for key in conflict_keys:
                conflict_dict[key] = max(self.doc_scores_dict[field][key], self.merged_scores[key])

            self.merged_scores.update(self.doc_scores_dict[field])
            self.merged_scores.update(conflict_dict)

        score_order_list = sorted(self.merged_scores.items(), key=itemgetter(1), reverse=True)

        return self.build_output(len(score_order_list),
                                 offset,
                                 min(len(score_order_list), counts),
                                 score_order_list[offset:offset + counts])

    @staticmethod
    def build_output(total, offset, counts, scores_dict):
        """
        Build output format
        :param total:
        :param offset:
        :param counts:
        :param scores_dict:
        :return:
        """
        doc_list = []
        for doc in scores_dict:
            doc_list.append(Document(doc[0], doc[1]))

        return {
            "numFound": total,
            "start": offset,
            "count": counts,
            "docs": doc_list
        }
