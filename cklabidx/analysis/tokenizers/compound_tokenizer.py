#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import re

from cklabidx.analysis.tokenizers.base_tokenizer import Tokenizer


class CompoundTokenizer(Tokenizer):
    """
    CompoundTokenizer
    """

    def generate_tokens(self, text):
        """
        Break input text with whitespace and NGram.

        :param text: Input text to be tokenize
        :return: A list of tokens
        """

        # TODO: Add compound tokenizer
        tokens = []  # Dummy value
        return tokens
